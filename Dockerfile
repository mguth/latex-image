FROM sauerburger/pdflatex:2020plus

## ensure locale is set during build
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN pip install PyPDF2 reportlab webcolors click

# COPY pdftk_installer.sh .

RUN apt update && \
    # chmod +x pdftk_installer.sh && \
    #./pdftk_installer.sh && \
    apt-get -y update && \
    apt-get -y install pdftk && \
    apt-get -y install latexdiff
